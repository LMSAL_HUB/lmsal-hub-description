

# LMSAL HUB documentation
===============================

   Edited by Juan Martinez-Sykora 

   Also available in pdf and EPUB formats.

   Contents:

[vision](docs/source/vision.md)

[structure](docs/source/structure.md) describes phylosophy behind, groups, subgroups and repositories.

[git](docs/source/git.md) provides a good manners of using git commands.

[gitlab](docs/source/gitlab.md) provides a short description of relevant features in gitlab.

[Preparing a repository](docs/source/repo.md).


## Working at LMSAL: 

In order to be able to use git, you need to set the following enviroments: 

   	 setenv https_proxy https://proxy.lmsal.com:80/
	 setenv http_proxy http://proxy.lmsal.com:80/


Same documentation can be found in the [readthedocs format](https://lmsal-hub-description.readthedocs.io/en/latest/index.html)