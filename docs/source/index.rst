.. LMSAL hub documentation master file, created by
   sphinx-quickstart on Sun Apr 22 15:50:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

LMSAL HUB documentation
===============================

.. only:: html

   Edited by Juan Martinez-Sykora

   Last updated on |today|.

   Also available in `pdf <https://gitlab.com/LMSAL_HUB/lmsal-hub-description/blob/master/docs/build/latex/LMSAL_hub_desc.pdf>`_ .

   Contents:

.. toctree::
   :numbered:
   :maxdepth: 2

   vision
   structure
   git
   gitlab
   repo


.. Indices and tables
.. ==================
..
.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
