
# LMSAL_HUB: How to prepare a repository

A repository should have at minimum a README file, a LICENSE file and documentation. The later can be easily created with the gitlab wiki, or using notebook instructions (and converted, for instance, to html or pdf using readthedocs) in a ``docs" folder.

********
Jupyter notebook instructions can be easily used to create readthedocs and should be located in the docs folder of the hosting repository.
********

A repository should be a self-consistent code.

Python repositories should have a setup.py [file](https://gitlab.com/LMSAL_HUB/sims_hub/eb_hub/atom_py/blob/master/setup.py) and in each folder within the
repository that contains the code an  __init__.py [file](https://gitlab.com/LMSAL_HUB/sims_hub/eb_hub/atom_py/blob/master/at_tools/__init__.py).

#### Install a repository


With this type of setup, it is very easy to install a new Python library (after
cloning the repository).  

      python setup.py install

##### Non-root install

If you do not have write permission to your Python packages directory, use the following option with setup.py:

      python setup.py install --user

This will install the Python library under your home directory (typically ~/.local).

##### Developer install

If you want to install the library but also actively change the code or contribute to its development, it is recommended that you do a developer install instead:

      python setup.py develop

This will set up the package such as the source files used from the git repository (only a link to it is placed on the Python packages directory). It can also be combined with the --user flag for local installs.
