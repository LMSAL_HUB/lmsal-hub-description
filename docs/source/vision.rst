.. _inst_ovw:

*******************
LMSAL_HUB Vision
*******************

LMSAL_HUB is a gitlab group for solar physics focused on maintaining, sharing,
distributing, debugging, and providing documentation to any code
developed at Lockheed Martin Solar and Astrophysical Lab
(LMSAL) or Bay Area Environmental Research Institute (BAERI). The repositories are mainly, but not only, in Python. The repositories
can be used also as a platform for develop the codes to other more versatile and state-of-the-art
repositories (e.g., sunpy, astropy or irispy).

The repositories could be a collection of different scripts, codes and classes with
various degrees of portability and capabilities. Each repository will contain requirements, installation instructions and
other useful information either in the README files, wiki or associated
readthedocs documents.
