# LMSAL_HUB Structure (gitlab)

LMSAL_HUB is a gitlab group which contains subgroups, repositories, libraries,
Scripts, documentation and other related materials.

In the LMSAL_HUB group you will find different subgroups related to
instruments, projects or subjects. Each of these subgroups may contain other subgroups, repositories, libraries,
scripts and/or documentation.

As mentioned in the vision section, e once the projects in the group are mature enough, they might be moved to elaborate and
state-of-the-art repositories (e.g., sunpy, astropy or irispy). For this reason,
each subgroup will have a repository which could be used as platform for the integration.

```mermaid
graph LR
    A[LMSAL HUB]-->B[AIA HUB];
    A-->C[IRIS HUB];
    A-->D[MUSE HUB];
    A-->E[SIMULATIONS HUB];
    E-->F[BIFROST HUB];
    E-->G[RADYN HUB];
    E-->H[EBYSUS HUB];
    E-->I[COMMON HUB];
    C-->J(iris_lmsalpy);
    C-->K(iris2);
    C-->L(milan_iris_tools);
    style A fill:#f9f
    style B fill:#f9f
    style C fill:#f9f
    style D fill:#f9f
    style E fill:#f9f
    style F fill:#f9f
    style G fill:#f9f
    style H fill:#f9f
    style I fill:#f9f
```
