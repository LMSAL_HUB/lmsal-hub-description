.. _inst_ovw:

*******************
LMSAL_HUB: Gitlab minimum help
*******************

Gitlab allows to create `groups <https://gitlab.com/dashboard/groups?nav_source=navbar>`_ to work on various `projects <https://gitlab.com/dashboard/projects>`_.
Most of you have guest access to LMSAL HUB. Guests can
see the group structure, repositories and wiki, but not the code unless the repository is open to the public or similar.

.. Note:: Guests do not have `permission <https://gitlab.com/help/user/permissions.md>`_  to add new subgroups, add new repositories or commit changes

If you are interested to have access to a specific subgroup or repository
you need the permission of the owners or PIs of the projects.

Once you are a member of a subgroup you can, create branches, add
new repositories, or commit changes. Gitlab allows to `import a repository <https://gitlab.com/projects/new>`_
from, for instance, github.

Gitlab allows to have `protected branches <https://gitlab.com/LMSAL_HUB/sims_hub/eb_hub/atom_py/settings/repository>`_ which allow to secure stable branches secure and force developers to use merge requests.

In addition, Gitlab has integrated `pipeline configurations <https://gitlab.com/LMSAL_HUB/sims_hub/eb_hub/atom_py/settings/ci_cd>`_ useful for adding debugging options, rerun code after any substantial change in the code etc.

The PI or owner of the repository or group will manage them and be in charge of
the accessibility. If any of you are interested to have a new subgroup, contact
directly the owners of LMSAL HUB.

In case any of you want to create new repositories with no access to the
code for any of the owners of the LMSAL HUB, then you should create your
own private independent repository/group. This could be moved eventually
to LMSAL HUB if the accessibility status changes and, at least, the owners
or LMSAL HUB can have access.
