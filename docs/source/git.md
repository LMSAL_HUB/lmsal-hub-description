
# LMSAL_HUB Git commits

(i.e., Mind your Git manners)

The commit message is mainly for the other people, so they should be able
to understand it now and six months later.  Commit msg must only be one
sentence and should start with a tag identifier (see end of this section).

Use the imperative form of verbs rather than past tense when referring to
changes introduced by commit in question. For example "Remove property X",
not "Removed property X" and not "I removed...".

Use the following tags for commit msg::

    [ADD] : adding new feature
    [FIX] : a bug
    [DOC] : documentation only
    [REF] : refactoring that does not include any changes
            in features
    [FMT] : formatting only (spacing, removes blank lines etc.)
    [REP] : repository related changes (e.g., changes in the
            ignore list, remove files )

Example commit msg:

    [FIX] Adds missing initialization to tg array
    [REF] Removes unnecessary allocation for dynamic arrays

**************
Avoid to commit more than one file every time. This facilitates to follow the commit documentation.
**************

**************
Avoid to do merge unless it is between different branches.
**************

**************
Avoid to add binary, executables files. In fact, mainly include only documentation, license, and code files.
**************
